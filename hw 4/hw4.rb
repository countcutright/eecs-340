# Aaron Cutright
# eecs 340 hw 5

def sparseTranspose_naive(r,c,v,m,n,k) #O(k+mn)
	r2 = Array.new(n+1, 0)
	c2 = Array.new(k, 0)
	v2 = Array.new(k, 0)

	idx = Array.new(k, 0) #index of each val in array_t
	row = 0
	for i in 0..k-1
		while i >= r[row+1]
			row = row + 1
		end
		idx[i] = c[i]*m + row
		puts "idx[#{i}] = #{idx[i]}   val=#{c[i]}  row=#{row}"
	end

	cnt = Array.new(m*n, 0)
	(k-1).downto(0) { |i|
		cnt[idx[i]] = cnt[idx[i]] + 1
	}

	for i in 1..m*n-1
		puts "cnt[#{i}] #{cnt[i]}"
		cnt[i] = cnt[i] + cnt[i-1]
	end

	(k-1).downto(0) { |i|
		v2[cnt[idx[i]]-1] = v[i]
		c2[cnt[idx[i]]-1] = idx[i] % m
		cnt[idx[i]] = cnt[idx[i]] - 1
	}

	row = 1 #indexing for r2
	prev_c = -1
	for i in 0..k-1
		if c2[i] < prev_c
			row = row + 1
			r2[row] = r2[row-1]
		end
		r2[row] = r2[row] + 1
		prev_c = c2[i]
	end
	return r2, c2, v2
end


def sparseTranspose_efficient(r,c,v,m,n,k) #O(k+m+n)
	r2 = Array.new(n+1)
	c2 = Array.new(k)
	v2 = Array.new(k)

	idx = Array.new(k) 
	row = 0
	prev_c = c[0]-1 
	for i in 0..k-1            #get location of each value in argument array
		if i >= r[row+1]       #needs to be while to detect row of all zeros
			row = row + 1
		end
		idx[i] = c[i] + row*n           
		puts "idx[#{i}] = #{idx[i]}   val=#{v[i]}  row=#{row}"
		prev_c = c[i]
	end

	cnt = Array.new(n)  
	for i in 0..n-1
		cnt[i] = 0
	end
	
	for i in 0..k-1                       #count number of elements per row
		cnt[idx[i] % n] = cnt[idx[i] % n] + 1
	end
	puts "cnt[0] #{cnt[0]}"
	for i in 1..n-1                       #accumulate the counts
		puts "cnt[#{i}] #{cnt[i]}"
		cnt[i] = cnt[i] + cnt[i-1]
		r2[i+1] = cnt[i]                  #build r2
	end
	r2[0] = 0                             #finish building r2
	r2[1] = cnt[0]                        #finish building r2

	(k-1).downto(0) { |i|                 #build v2 and c2
		v2[cnt[idx[i] % n] - 1] = v[i]
		c2[cnt[idx[i] % n] - 1] = idx[i] / n
		cnt[idx[i] % n] = cnt[idx[i] % n] - 1
	}
	return r2, c2, v2
end


r = [0,2,3,5]
c = [1,3,2,0,1]
v = [9,1,1,3,8]
m = 3 #rows
n = 4 #columns
k = 5 #non zero entries

result = sparseTranspose_naive(r,c,v,m,n,k)
puts "naive:     #{result}   (case 1)"
result = sparseTranspose_efficient(r,c,v,m,n,k)
puts "efficient: #{result}   (case 1)"


# case 2
c = [0,1,2,2,3]
result = sparseTranspose_naive(r,c,v,m,n,k)
puts "naive:     #{result}   (case 2)"
result = sparseTranspose_efficient(r,c,v,m,n,k)
puts "efficient: #{result}   (case 2)"
# [0,1,2,4,5] [2,2,0,1,0] [9,1,3,1,8]