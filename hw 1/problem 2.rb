
def match(arr1, arr2, x)
	if arr1[0] + arr2[0] > x
		return false
	end
	i = 0
	j = 0
	while (i < arr1.length && j < arr2.length) 
		if (arr1[i+1] + arr2[j] <= x)
			i = i + 1
		elsif (arr1[i] + arr2[j+1] <= x)
			j = j + 1
		else #can't increment either
			return false
		end
		if (arr1[i] + arr2[j] == x)
			return [i, j]
		end
	end
	return false
end

print match([1,2,4,7,11,13], [4,7,11,13], 15), "\n" #4,0
print match([1,2,4,7,11,12], [4,7,11,13], 16), "\n" #5,0
print match([1,2,4,7,11,13], [4,7,11,13], 16), "\n" #f
print match([4,5,6,13], [1,2,4,7,11,13], 13),  "\n" #2,3

