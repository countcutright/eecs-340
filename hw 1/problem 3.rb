# Aaron Cutright
# EECS 340, hw1, problem 3

def merge_sort(arr)
	return arr if arr.length <= 1
	midp = (arr.length / 2).floor 
	left = merge_sort(arr[0..midp-1])
	right = merge_sort(arr[midp..(arr.length-1)])
	merge(left, right)
end

def merge(left, right)
	if left.empty?
		right
	elsif right.empty? 
		left
	elsif left.first < right.first
		[left.first] + merge(left[1..left.length], right)
	else
		[right.first] + merge(left, right[1..right.length])
	end
end

def insertion_sort(arr)
	for j in 1..(arr.length-1)
		key = arr[j]
		i = j-1
		while i >= 0 and arr[i] > key
			arr[i+1] = arr[i]
			i = i - 1
		end
		arr[i+1] = key
	end
	return arr
end

def generate_random_array(length, min, max)
	arr = Array.new(length)
	for i in 0..(length-1)
		arr[i] = rand(min..max)
	end
	return arr
end

def generate_sorted_array(length, ascending)
	arr = Array.new(length)
	for i in 0..(length-1)
		if(ascending)
			arr[i] = i;
		else
			arr[i] = (length-1) - i
		end
	end
	return arr
end

trials = 10
n = [4, 16, 64, 256, 1024, 2048]
insertion_times_rand = Array.new
merge_times_rand = Array.new
insertion_times_worst = Array.new
merge_times_worst = Array.new

ascending = true
descending = false

# do all of the trials
for trial in 0..trials-1
	# generate test arrays
	g_rand = Array.new
	g_worst = Array.new
	for i in 0..n.length-1
		g_rand[i] = generate_random_array(n[i], 0, n.length-1)
		g_worst[i] = generate_sorted_array(n[i], descending)
	end

	insertion_trial_rand = Array.new
	merge_trial_rand = Array.new
	insertion_trial_worst = Array.new
	merge_trial_worst = Array.new

	for i in 0..n.length-1
		# make clones before starting timers
		mr = g_rand[i].clone
		ir = g_rand[i].clone
		mw = g_worst[i].clone
		iw = g_worst[i].clone

		# merge sort trial for each random generated array in g
		start = Time.now.to_f
		merge_sort(mr)
		finish = Time.now.to_f
		merge_trial_rand[i] = ((finish - start)*1000.0*1000).round(4) # convert time to us

		# insertion sort trial for each random generated array in g
		start = Time.now.to_f
		insertion_sort(ir)
		finish = Time.now.to_f
		insertion_trial_rand[i] = ((finish - start)*1000.0*1000).round(4) # convert time to us

		# merge sort trial for each sorted generated array in g
		start = Time.now.to_f
		merge_sort(mw)
		finish = Time.now.to_f
		merge_trial_worst[i] = ((finish - start)*1000.0*1000).round(4) # convert time to us
		
		# insertion sort trial for each sorted generated array in g
		start = Time.now.to_f
		insertion_sort(iw)
		finish = Time.now.to_f
		insertion_trial_worst[i] = ((finish - start)*1000.0*1000).round(4) # convert time to us
	end
	insertion_times_rand[trial] = insertion_trial_rand
	merge_times_rand[trial] = merge_trial_rand
	insertion_times_worst[trial] = insertion_trial_worst
	merge_times_worst[trial] = merge_trial_worst
end

# write data to file
open('report.csv', 'w') do |f|
	f.puts "n,insertion sort (random),merge sort (random),==,insertion sort (reverse sorted), merge sort (reverse sorted)\n"
    for i in 0..n.length-1
		for t in 0..trials-1
			#print "#{n[i]},#{insertion_times[t][i]},#{merge_times[t][i]}\n"
			f.puts "#{n[i]},#{insertion_times_rand[t][i]},#{merge_times_rand[t][i]},-,#{insertion_times_worst[t][i]},#{merge_times_worst[t][i]}\n"
		end
	end
end
